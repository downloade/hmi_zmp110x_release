#include "awtk.h"
#include "base/main_loop.h"
#include "pc_uart_debug.h"
#include "../serial/iostream_serial.h"

static com_info_t com_info;
static tk_iostream_t* __g_uart_handle = NULL;

static ret_t uart_check_interval(const timer_info_t* t)
{
    static bool_t i = 0;
    static char rx_text[2][256];

    if (__g_uart_handle != NULL) {
        ret_t ret = tk_iostream_serial_wait_for_data(__g_uart_handle, 10);
        if (ret == RET_OK) {
            int32_t len = tk_istream_read(__g_uart_handle->get_istream(__g_uart_handle), rx_text[i], 256);
            
            event_queue_req_t req;
            memset(&req, 0x00, sizeof(req));

// #define EVT_USER_RX_DATA EVT_USER_START + 1

// /* 串口屏通信接口 */
// int uart_data_send(const uint8_t* p_data, uint32_t nbytes);

            req.event.type = EVT_USER_START + 1;
            req.event.target = window_manager();
            req.event.time = time_now_ms();
            req.event.size = len;
            req.event.native_window_handle = rx_text[i];

            i = !i;
            main_loop()->queue_event(main_loop(), &req);

        } else if (ret == RET_TIMEOUT) {
            return RET_REPEAT;
        }
    }
    return RET_REPEAT;
}

static ret_t on_open_uart(void* ctx, event_t* e)
{
    combo_box_t* combo_box = COMBO_BOX(ctx);
    __g_uart_handle = tk_iostream_serial_create(com_info.com_info[combo_box->value].com);
    tk_iostream_serial_config(__g_uart_handle, PC_UART_BAUDRATE, PC_UART_BYTESIZE,
                              PC_UART_PARITY,  PC_UART_STOPBITS, PC_UART_FLOWCONTROL);
    
    if (__g_uart_handle) {
        free(com_info.com_info);
        log_debug("open com success!\n");
        timer_add(uart_check_interval, __g_uart_handle, CHECK_INTERVAL);


        window_close(WIDGET(ctx)->parent);
    } else {
        log_debug("open com failed!\n");
    }
    return RET_OK;
}

static ret_t create_uart_seldlg(const timer_info_t* time)
{
    widget_t* wm = window_manager();
    widget_t* dlg = dialog_create(wm, wm->w * 0.15, wm->w * 0.35, wm->w * 0.7, wm->h * 0.3);
    widget_t* open_uart = button_create(dlg, dlg->w * 0.35, dlg->h * 0.6, dlg->w * 0.3, dlg->h * 0.2);
    widget_t* combo_box = combo_box_create(dlg, dlg->w * 0.15, dlg->h * 0.2, dlg->w * 0.7, dlg->h * 0.2);

    widget_set_prop_str(dlg, "highlight", "default(alpha=80)");

    combo_box_set_localize_options(combo_box, FALSE);
    widget_set_prop_bool(combo_box, "readonly", TRUE);

    char com_name[60] = {0};
    for(int i = 0; i < com_info.com_count; i++) {
        tk_snprintf(com_name, 60, "%s %s", com_info.com_info[i].com, com_info.com_info[i].com_name);
        combo_box_append_option(combo_box, i, com_name);
    }
    combo_box_set_selected_index(combo_box, 0);

    widget_set_text_utf8(open_uart, "OPEN");
    widget_on(open_uart, EVT_CLICK, on_open_uart, combo_box);
    return RET_OK;
}


#if defined(WIN32) && !defined(MINGW)
int uart_data_send(const uint8_t* p_data, uint32_t nbytes)
{
    tk_ostream_write_len(__g_uart_handle->get_ostream(__g_uart_handle), p_data, nbytes, 500);
    return RET_REPEAT;
}
#endif


/* C++ func */
void get_uart_info(com_info_t* com_info);

void pc_uart_debug_init()
{
#if defined(WIN32) && !defined(MINGW)
    get_uart_info(&com_info);

    tk_socket_init();
    platform_prepare();

    timer_add(create_uart_seldlg, NULL, 50);
#endif
}

void pc_uart_debug_deinit()
{
#if defined(WIN32) && !defined(MINGW)
    tk_socket_deinit();
#endif
}
