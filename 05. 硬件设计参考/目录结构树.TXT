
├─01. 硬件设计指南
│      00. 【使用说明书】HMI-ZMP1106-R7C使用说明书.pdf
│      01. 【芯片数据手册】DS_ZMP110X_1.1.01_SC.pdf
│      02. 【芯片硬件设计指南】ZMP1107硬件设计指南V1.0.01.pdf
│      
├─02. 原理图-PDF格式
│  ├─01. 原理图
│  │      HMI-ZMP1106 Rev.A.pdf
│  │      
│  └─02. PCB位号图
│          HMI-ZMP1106 Rev.A.pdf
│          
├─03. 封装库
│      ZMP110x.PcbLib
│      ZMP110x.SCHLIB
│      
├─04. 机械尺寸
│      HMI-ZMP1106 Rev.A机械尺寸.pdf
│      HMI-ZMP1106.DWG
│      
├─05. 可选配件
│      （7寸液晶显示屏）7.0inch 800X480 TN RGB AML78550B-B37 主 Shenzhen Amelin Electronic Technology Co. Ltd(1).pdf
│      （7寸电容触摸屏）7.0 inch 1024X600或800X480 CTP GT911.pdf
│      
└─06. 生产文件
    │  生产文件说明.txt
    │  
    ├─01. HMI-ZMP1106_CAM
    │      Fabrication Testpoint Report for HMI-ZMP1106 .ipc
    │      Fabrication Testpoint Report for HMI-ZMP1106 .REP
    │      HMI-ZMP1106 -macro.APR_LIB
    │      HMI-ZMP1106 -RoundHoles.TXT
    │      HMI-ZMP1106 -SlotHoles.TXT
    │      HMI-ZMP1106 .apr
    │      HMI-ZMP1106 .DRR
    │      HMI-ZMP1106 .EXTREP
    │      HMI-ZMP1106 .GBL
    │      HMI-ZMP1106 .GBO
    │      HMI-ZMP1106 .GBP
    │      HMI-ZMP1106 .GBS
    │      HMI-ZMP1106 .GD1
    │      HMI-ZMP1106 .GG1
    │      HMI-ZMP1106 .GM1
    │      HMI-ZMP1106 .GM2
    │      HMI-ZMP1106 .GP1
    │      HMI-ZMP1106 .GP2
    │      HMI-ZMP1106 .GTL
    │      HMI-ZMP1106 .GTO
    │      HMI-ZMP1106 .GTP
    │      HMI-ZMP1106 .GTS
    │      HMI-ZMP1106 .LDP
    │      HMI-ZMP1106 .REP
    │      HMI-ZMP1106 .RUL
    │      Pick Place for HMI-ZMP1106 .txt
    │      Status Report.Txt
    │      
    ├─02. HMI-ZMP1106_SMT
    │      HMI-ZMP1106 .GBP
    │      HMI-ZMP1106 .GTP
    │      Pick Place for HMI-ZMP1106 .txt
    │      
    ├─03. HMI-ZMP1106_BOM
    │      （BOM可选）HMI-ZMP1106 Rev.A.xls
    │      （HMI-ZMP1106-R7C生产使用）HMI-ZMP1106 Rev.A.xls
    │      （完整BOM）HMI-ZMP1106 Rev.A.xls
    │      
    └─04. 坐标文件
            Pick Place for HMI-ZMP1106 .csv
            Pick Place for HMI-ZMP1106 .txt
            
