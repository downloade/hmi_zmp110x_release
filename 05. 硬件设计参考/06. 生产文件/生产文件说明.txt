该生产文件可用于嘉立创直接制板贴片；
以下为嘉立创PCB下单信息：

基本信息：

板材类别：FR-4
板子尺寸：16.68cm*15.6cm
板子层数：4
产品类型：工业/消费/其他类电子产品

PCB工艺信息：
拼板款数：1
板子厚度：1.6
外层铜厚：1盎司
内层铜厚：1盎司
层压顺序：我的文件中已有层压顺序
需要阻抗：需要
层压结构：JLC041611-3313（原2313结构）
最小孔径：0.2/0.45mm

其他选项根据需求选择
